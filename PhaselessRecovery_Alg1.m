%% gauss_model_recovery

% Author: Jonathan Mousley
% Date: 6/28/2021
% Summary: contains functions to recover sample given parameters N, beta,
%   alpha and measurements. Also contains routine to produce measurements
%   given appropriate paramters and original x_hat
% Inputs: 
%       N: dimension of sample
%       beta: mask support parameter
%       alpha: sub-sampling parameter (must divide N)
%       noise_factor: multiped by random normal noise in measurements
%       measurements: sub-sampled measurements according to gaussian model
% Outputs: 
%       x_hat_recover: estimated sample

%% Initialize parameters and dependents
%[N,beta,alpha] = find_param(30,80); % random choice of parameters that fit constraints

N = 50;
alpha = 1;
beta = 4;

%% Numerical Experiments
% parameters
snr_lower = 10;
snr_upper = 10;
num_trials = 1;

% pull data for different snr values and fixed N,beta,alpha
rec_error_vs_snr_data = test_gauss_recovery(N,beta,alpha,snr_lower,snr_upper,num_trials);
%writematrix(rec_error_vs_snr_data,'rec_error_vs_snr_alph10.txt'); % each col corresponds to snr value, varying rows correspond to trials

snr_values = snr_lower:10:snr_upper; % a row
avg_errors = mean(rec_error_vs_snr_data,1); % a row
avg_rec_error_vs_snr = [snr_values' avg_errors'];
%writematrix(avg_rec_error_vs_snr,'avg_rec_error_vs_snr_alph10.txt');

% display fig
figure; plot(avg_rec_error_vs_snr(:,1), avg_rec_error_vs_snr(:,2), '-+', 'linewidth', 2); hold on
axis([0 65 -80 10]); grid
xlabel( 'Noise Level in SNR (dB)', 'interpreter', 'latex', 'fontsize', 14 )
ylabel( 'Reconstruction Error (in dB)', 'interpreter', ...
                                                'latex', 'fontsize', 14 )
title( 'Robustness to Measurement Noise: $N=, \alpha = , \beta = $', ...
                                 'interpreter', 'latex', 'fontsize', 14 )
legend( {'Gauss Model'}, ...
         'location', 'southwest', 'interpreter', 'latex', 'fontsize', 14 )

 % save figures
 saveas(gcf,'Figures\noise_robustness_alph10.fig');
 saveas(gcf,'Figures\noise_robustness_alph10.png');
 saveas(gcf,'Figures\noise_robustness_alph10.pdf');

 
%% Test recovery routine
% addnoise = 1; % 1 = yes, 0 = no
% snr = 50; % signal to noise ratio
% x_hat = randn(N,1) + 1i*randn(N,1); % original x_hat (to be recovered)
% gauss_matrix = randn(N,2*beta + 1) + 1i*randn(N,2*beta + 1); % g_n's, row j is g_j^T
% 
% % dependents
% s = 2*beta + 1; % mask support cardinality
% mask_supp = supp(N,beta); % support array
% measurements = gauss_model_measurements(x_hat,N,alpha,gauss_matrix,mask_supp,addnoise,snr); %experimental measurements
% 
% % output parameters
% fprintf("N = %d\nbeta = %d\ns = %d\nalpha = %d\naddnoise = %d\nsnr = %d\n",N,beta,s,alpha,addnoise,snr);
% 
% % recover x_hat via lifting and eigenvector synchronization
% x_hat_recover = gauss_recover_sample(N,beta,alpha,measurements,gauss_matrix,mask_supp);
% 
% % scale x_hat by appropriate unimodular constant prior to comparison
% x_hat = (x_hat_recover(1)/x_hat(1))*x_hat;
% 
% % compare x_hat and recovered x_hat
% fprintf("\nerrordB = %e\n",10*log10(norm(x_hat-x_hat_recover)^2/norm(x_hat)^2));
% fprintf("norm of resid = %e\n",norm(x_hat - x_hat_recover));
% fprintf("norm of resid / norm of expected = %f\n",norm(x_hat-x_hat_recover)/norm(x_hat));

% for trials, everything stays same except for x_hat and snr
% snr range: 10 - 60, intervals of 10
% 100 trials for each snr
% N: something in 100 - 200 range
% overlap: 10 - 50%, 10% < (s-alpha)/s < 50%
% conditions for beta: have s ~ order of N/10
% maybe start with assuming N/alpha ~ 10-20, as that's how many LEDs
% practioners have
% given choice of N, this then constrains s
%% functions

% scale eigenvector of block to correct magnitude

% test recovery method for fixed N,beta,alpha, stores errordB in array
function y = test_gauss_recovery(N,beta,alpha,snr_lower,snr_upper,num_trials)
    % dependents
    mask_supp = supp(N,beta); % support array

    %define range of snr values
    snr_values = snr_lower:10:snr_upper;
    
    %col j corresponds to trials run for snr_values(j)
    errordB_array = zeros(num_trials,size(snr_values,2));
    for k = 1:size(snr_values,2)
        for j=1:num_trials
            fprintf("Beginning snr #: %d, trial #: %d\n",k,j);
            tic
            x_hat = randn(N,1) + 1i*randn(N,1); % original x_hat (to be recovered)
            gauss_matrix = randn(N,2*beta + 1) + 1i*randn(N,2*beta + 1); % g_n's, row j is g_j^T
            measurements = gauss_model_measurements(x_hat,N,alpha,gauss_matrix,mask_supp,1,snr_values(k)); %experimental measurements
            x_hat_recover = gauss_recover_sample(N,beta,alpha,measurements,gauss_matrix,mask_supp);
            
            % scale x_hat by appropriate unimodular constant prior to comparison
            x_hat = (x_hat_recover(1)/x_hat(1))*x_hat;
            
            % store recovery error
            errordB_array(j,k) = 10*log10(norm(x_hat-x_hat_recover)^2/norm(x_hat)^2);
            fprintf("errordB = %f\n",errordB_array(j,k));
            toc
             fprintf("\n");
            
        end
    end
    
    y = errordB_array;
end

function y = recover_mag(block)
    [evec, eval] = eig(block);
    [l_eval,l_index] = max(diag(eval));
    l_evec = evec(:,l_index);
    
    y = (sqrt(l_eval)/norm(l_evec))*l_evec;
end

% create psuedo random array of sutiable choices for parameters, with N between lower and upper
function [y1,y2,y3] = find_param(lower, upper)
 N = randi([lower upper],1,1);
 
 param_not_found = 1;
 count = 1;
 while param_not_found
   %inital choice
   beta = randi([1 (floor(N/2)-1)],1,1);
   alpha = randi([2 2*beta],1,1); 
   s = 2*beta +1;
   if (N >= (2*s-alpha)*alpha) && mod(N,alpha) == 0
       param_not_found = 0;
   else
       count = count +1;
   end
   
   if count >= 1000
        N = randi([lower upper],1,1);
        count = 1;
   end
 end
 y1 = N;
 y2 = beta;
 y3 = alpha;
end

% recover perfect cascading block from shifted cascading block
function y = perfect_cascade(x_x_star,beta,alpha)
    N = size(x_x_star,1);
    s = 2*beta + 1;
    if (mod(beta,alpha) ~= 0) %first block not full
        x_x_star = [zeros(N,mod(beta,alpha)) x_x_star];
        x_x_star = [zeros(mod(beta,alpha),N+mod(beta,alpha)); x_x_star];
        x_x_star(1+mod(beta,alpha):s,1:mod(beta,alpha)) = x_x_star(1+mod(beta,alpha):s,N+1:N+mod(beta,alpha));
        x_x_star(1:mod(beta,alpha),1+mod(beta,alpha):s) = x_x_star(1+mod(beta,alpha):s,1:mod(beta,alpha))';
        x_x_star(1:mod(beta,alpha),1:mod(beta,alpha)) = x_x_star(N+1:N+mod(beta,alpha),N+1:N+mod(beta,alpha));
    end
   
    if (mod((N - (s-mod(beta,alpha))),alpha) ~= 0) %last block not full
        %add alpha - mod(N-(s-mod(beta,alpha)),alpha) cols, rows to right
        %and bottom of x_x_star respectively
        overlap_count = alpha - mod(N-(s-mod(beta,alpha)),alpha);
        %bottom
        x_x_star = [x_x_star; zeros(overlap_count,size(x_x_star,1))];
        %right
        x_x_star = [x_x_star zeros(size(x_x_star,1),overlap_count)];
        
        %fill in cols of last block
        x_x_star(N+mod(beta,alpha)-(s-overlap_count)+1:N+mod(beta,alpha),size(x_x_star,1)-overlap_count+1:size(x_x_star)) = x_x_star(N+mod(beta,alpha)-(s-overlap_count)+1:N+mod(beta,alpha),1+mod(beta,alpha):overlap_count+mod(beta,alpha));
        %fill in rows of last block
        x_x_star(size(x_x_star,1)-overlap_count+1:size(x_x_star),N+mod(beta,alpha)-(s-overlap_count)+1:N+mod(beta,alpha)) = x_x_star(N+mod(beta,alpha)-(s-overlap_count)+1:N+mod(beta,alpha),size(x_x_star,1)-overlap_count+1:size(x_x_star))';
        
        %fill bottom right corner of bottom block
        x_x_star(size(x_x_star,1)-overlap_count+1:size(x_x_star,1),size(x_x_star,1)-overlap_count+1:size(x_x_star,1)) = x_x_star(1:overlap_count,1:overlap_count);
    end
    
    y = x_x_star;
end

% compatability of block eigenvectors
function [recovered_entries, adjusted_evec_2] = block_compatability(alpha,s,evec_1,evec_2)
    overlap_1 = evec_1(alpha + 1:s);
    overlap_2 = evec_2(1:s-alpha);
   
    %overlap magnitude scaling
    factor1 = (norm(overlap_1)/norm(overlap_2));
    evec_2 = factor1*evec_2;
    
    %overlap phase
    factor2 = ((overlap_2'*overlap_1)/(norm(overlap_2)*norm(overlap_1)));
    
    evec_2 = factor2*evec_2;
    
    adjusted_evec_2 = evec_2;
    
    %delete overlap entries from evec_2
    evec_2(1:s-alpha) = [];
    
    recovered_entries = evec_2;
end

% compute ordering of variables for block structure
function y = variable_order(N,beta)
    s = 2*beta + 1;
    v_indices = zeros(N*(2*s - 1),2);

    %set all indices
    for n = 0:N-1
        v_indices((1:2*s-1) + n*(2*s-1),1) = N - beta - n;
        for j = 0+n*(2*s-1):2*s-2+n*(2*s-1)
            v_indices(j+1,2) = beta-n-j+n*(2*s-1);
        end
    end
    y = mod(v_indices,N);
end

% find indices of variables in block
function y = block_order(v_indices,my_bb_index,N,s)
    b_indices = circshift(v_indices,-my_bb_index+1);
    b_indices(2*s*(s-1)+1:N*(2*s-1),:) = [];
    
    y = b_indices;
end

% compute measurement data given x_hat, N, and alpha (returns sub-sampled
% measurements)
function y = gauss_model_measurements(sample, N, alpha, g_matrix, supp, addnoise, snr)
    my_measurements = zeros(N, N/alpha);
    for j = 0:N-1
        for k = 0:N/alpha - 1
            S_ell_x_hat = circshift(sample,k*alpha);
            my_measurements(j+1,k+1) = abs(S_ell_x_hat(supp+1).'*g_matrix(j+1,:).')^2;
        end
    end
    signal = reshape(my_measurements,N^2/alpha,1);
    
    if (addnoise)
        signal_power = norm(signal)^2/(N^2/alpha);
        noise_power = signal_power/(10^(snr/10));
        noise = sqrt(noise_power)*randn( size(signal));
        signal = signal + noise;
    end
    y = signal;
end

% compute coeff in front of x_k*conj(x_j) on row n for gauss e_n model
function y = gauss_coeff(k,j,n,supp,g_matrix)
    [is_k_in_supp,supp_index_k] = ismember(k,supp);
    [is_j_in_supp,supp_index_j] = ismember(j,supp);
    if (is_k_in_supp && is_j_in_supp)
        y = g_matrix(n+1,supp_index_k)*conj(g_matrix(n+1,supp_index_j));
    else
        y = 0;
    end 
end

% returns support vector given N and beta
function y = supp(N,beta)
    my_supp = (0:N-1).';
    my_supp = mod(my_supp,N);
    my_supp((beta+2):(N-beta)) = [];
    
    y = my_supp;
end

% recovers x_hat via lifting and angular synchronization according to gauss
% model
function y = gauss_recover_sample(N,beta,alpha,measurements,g_matrix, supp)
    
    % define necessary variables
    s = 2*beta + 1;
    
    % verify choice of N, beta, alpha allows lifted system to be solved
    if (alpha >= s)
        error("Improper parameters: alpha >= s.\n alpha = %d\ns = %d\n",alpha,s);
    end

    if (N < (2*s-alpha)*alpha)
        error("Improper parameters: (2s-alpha)*alpha > N.\n N = %d\n (2s-alpha)*alpha = %d\n",N,alpha*(2*s-alpha));
    end    

    % order variables for block structure
    variable_indices = variable_order(N,beta);

    % index of (beta,beta) row in variable_indices
    bb_index = 1 + N*(2*s-1) - 4*beta*(s-1);

    % get indices of variables in first block
    block_indices = block_order(variable_indices,bb_index,N,s);
    
    % Lift non-linear system %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % compute block B_gauss
    B_gauss = zeros(N,2*s*(s-1));
    for j = 0:2*s*(s-1)-1
        for n = 0:N-1
            B_gauss(n+1,j+1) = gauss_coeff(block_indices(j+1,1),block_indices(j+1,2),n,...
                supp,g_matrix);
        end
    end

    % construct entire matrix C
    C_gauss = zeros(N*(N/alpha),N*(2*s-1));
    for r = 0:N/alpha - 1
        for j = bb_index-1+r*alpha*(2*s-1):bb_index + 2*s*(s-1)-2+r*alpha*(2*s-1)
            C_gauss(1+r*N:N+r*N,mod(j,N*(2*s-1))+1) = B_gauss(:,j-bb_index+2-r*alpha*(2*s-1));
        end
    end

    % remove free variables due to sub-sampling
    zero_cols_C_gauss = all(C_gauss == 0);
    variable_indices(zero_cols_C_gauss,:) = [];
    C_gauss(:,zero_cols_C_gauss) = [];
    lifted_sol = C_gauss\measurements;
    
    % display coefficient matrix
    figure(1);spy(C_gauss); title({"Gauss Recovery: Coefficient matrix,C_{gauss}"...
    , "size: " + size(C_gauss,1) + " by " + size(C_gauss,2) + ", rank: " + rank(C_gauss),...
    "N = " + N + ", beta = " + beta + ", s  = " + s...
    + ", alpha = " + alpha});

    figure(2);spy(C_gauss.'); title({"Gauss Recovery: Coefficient matrix,C_{gauss}^T"...
    , "size: " + size(C_gauss,1) + " by " + size(C_gauss,2) + ", rank: " + rank(C_gauss),...
    "N = " + N + ", beta = " + beta + ", s  = " + s...
    + ", alpha = " + alpha});

    writematrix(C_gauss,'C_gauss')

    figure(4);spy(C_gauss.'*C_gauss); title({"Gauss Recovery: Coefficient matrix,C_{gauss}^TC_{gauss}"...
    , "size: " + size(C_gauss,1) + " by " + size(C_gauss,2) + ", rank: " + rank(C_gauss),...
    "N = " + N + ", beta = " + beta + ", s  = " + s...
    + ", alpha = " + alpha});
    % Construct xx* matrix (with zeros in free variable spots)
    x_x_star = zeros(N,N);
    for j = 0:size(lifted_sol,1)-1
        x_x_star(variable_indices(j+1,1)+1, variable_indices(j+1,2)+1) = lifted_sol(j+1);
    end
    
    % display adjacency matrix xx*
%     figure(2);spy(x_x_star); title({"Gauss Recovery: 'Adjacency Matrix' xx^*","N= "...
%         + N + ", beta = " + beta + ", s  = " + s + ", alpha = " + alpha});

    % make noisey xx* hermitian
    x_x_star = 0.5*(x_x_star + x_x_star');

    % Eigenvector synchronization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % define vector to store recovered values
    x_hat_recover = zeros(N,1);

    % redefine x_x_star so all diagonal blocks are not cut off
    x_x_star = perfect_cascade(x_x_star,beta,alpha);

    % display reformated xx*, where all overlaped blocks are full
%     figure(3);spy(x_x_star); title({"Gauss Recovery: Perfect Cascade 'Adjacency Matrix' xx^*","N= "...
%         + N + ", beta = " + beta + ", s  = " + s + ", alpha = " + alpha});

    %recover first s, store in x_hat_recover
    evec_first = recover_mag(x_x_star(1:s,1:s));
    x_hat_recover(1:s) = evec_first;

    %move down block diagonal, recover alpha componenets at a time
    for j = 0: ceil((N-s)/alpha)-1
        lower = 1+(j+1)*alpha;
        upper = s+(j+1)*alpha;
        evec_second = recover_mag(x_x_star(lower:upper,lower:upper));
        [x_hat_recover(s+1+j*alpha:s+1+(j+1)*alpha -1),evec_second] = block_compatability(alpha,s,evec_first,evec_second);
        evec_first = evec_second;
    end

    % remove extra components recovered, shift x_hat_recover to account for
    % first block overlap
    y = circshift(x_hat_recover(1:N),-mod(beta,alpha));
    
end
