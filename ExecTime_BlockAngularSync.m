clear; close all; clc

% For pretty pictures
% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',14);
set(0,'defaulttextfontsize',14);
set(0, 'DefaultLineLineWidth', 2);
set(0,'defaultTextInterpreter','latex');
set(0,'defaultLegendInterpreter','latex');



%%%%
% This generates a plot of execution time vs N for fixed s and alpha
%%%%



% For each of these N values
Nvals = [50; 150; 300; 600; 1200; 2400];

% Stores times and errors here
etime = zeros(length(Nvals), 1);
error = zeros(length(Nvals), 1);

% No. of trials
ntrials = 50;

% SNR for added noise
snr = 40;

fprintf( '\n\n' );

for iNval = 1:length(Nvals)

%% Problem Parameters
N = Nvals(iNval);               % Problem/signal size
beta = 5;                       % |supp(m^)| = 2 beta + 1 = s
s = 2*beta+1;
alpha = 5;                      % Shift size (# of indices to shift by)
shifts = (1:alpha:N).';         % shifts


%% Adjacency matrix

% for circular indexing...
circidx = @(x) mod(x-1, N) + 1;

% We will first construct an adjacency matrix
adj_mat = sparse(N, N);

for ix = 1:length(shifts)
    ell = shifts(ix)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    adj_mat(j,k) = adj_mat(j,k) + 1;
end

% No. of unknowns
nvars = nnz(adj_mat);

% Order of indexing variables (column major of non-zero entries of above
% adjacency matrix)
[rowidx, colidx] = find(adj_mat);
varorder = sparse(rowidx(:), colidx(:), (1:nvars).', N, N, nvars);


%% Trials loop
for itrial = 1:ntrials

%% Test "Signal" and rank-1 matrix

% Define (Fourier transforms of) true signal - (vector of interest)
xhat = randn(N, 1) + 1i*randn(N, 1);

Xhat = xhat*xhat';          % Rank - 1 matrix

% Add noise
sig_power = ( norm(Xhat(:))^2 ) / numel(Xhat);
noise_power = sig_power / (10^(snr/10));
noise_mat = sqrt(noise_power/2)*( randn(N) + 1i*randn(N) );
Xhat = Xhat + noise_mat;


% Hermitian Symmetrize
Xhat = (Xhat+Xhat')/2;


%% Here is our data matrix

T_X = sparse(N, N);

for ix = 1:length(shifts)
    ell = shifts(ix)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    T_X(j,k) = Xhat(j,k);
end


%% Perform angular synchronization in blocks

tic;

% Initialize solution vector
xhat_rec = zeros(N,1); 

% First block
    % find the block entries
    ell = shifts(1)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    block_entries = T_X(j,k);
    
    % Find e-vector
    [evec, eval] = eigs(block_entries, 1, 'largestabs');

    % Update solution corr. to first block
    u_prev = sqrt(eval)*evec;
    xhat_rec(j) = u_prev;

for ix = 2:length(shifts)
    % In each block...
    
    % find the block entries
    ell = shifts(ix)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    block_entries = T_X(j,k);
    
    % Find e-vector
    [evec, eval] = eigs(block_entries, 1, 'largestabs');    
    
    % Scale to correct magnitude
    u = sqrt(eval)*evec;
    
    % Extract overlapping portions
    u_prev_tilde = u_prev(end-(s-alpha)+1:end);
    u_tilde = u(1:(s-alpha));
    
    % Update magnitude of u
    u = ( norm(u_prev_tilde)/norm(u_tilde) )*u;
    
    % Update phase
    u = ( (u_tilde'*u_prev_tilde)/(norm(u_prev_tilde)*norm(u_tilde)) )*u;
    
    % Update components of solution vector
    xhat_rec(j) = u;
    
    % Store current e-vec for next pass
    u_prev = u;    

end

% Store execution time
etime(iNval,1) = etime(iNval,1) + toc/ntrials;

% ... and error

% Update for global phase factor
phase_factor = xhat_rec'*xhat/(xhat'*xhat);
xhat_rec = xhat_rec*phase_factor;

% Store average error
err = 10*log10( (norm(xhat-xhat_rec)^2) / (norm(xhat)^2) );
error(iNval,1) = error(iNval,1) + err/ntrials;


end

fprintf( '\n Block Angular Synchronization ' );
fprintf( '\n Avg. ang. sync. time for N=%d is %3.3e', N, etime(iNval,1) );
fprintf( '\n Avg. error (dB) for N=%d is %3.3f\n', N, error(iNval,1) );
%

end

fprintf( '\n\n' );


% Plot
figure; 
loglog(Nvals, etime(:,1), '-+', 'linewidth', 2); hold on
%
loglog(Nvals(3:5), 1e-5*Nvals(3:5), ':', 'linewidth', 2);
%
xlabel('Problem Size, $N$'); ylabel 'Execution Time (secs.)'
title 'Execution Time - Alg. 2; ($s=11, \alpha=5$)'
grid
legend( 'Computed (Block Ang. Sync.)', ...
        '$\mathcal{O}(N)$', 'location', 'northwest')
axis([30 4e3 8e-4 3e-1]); drawnow














%%%%
% This generates a plot of execution time vs s for fixed N and alpha
%%%%




% Problem size
N = 1200;

% For each of these s values
svals = [11; 51; 101; 301];

% Stores times and errors here
etime = zeros(length(svals), 1);
error = zeros(length(svals), 1);

% No. of trials
ntrials = 50;

% SNR for added noise
snr = 40;

fprintf( '\n\n' );

for isval = 1:length(svals)

%% Problem Parameters
s = svals(isval);               % Choose s value
beta = (s-1)/2;                 % |supp(m^)| = 2 beta + 1 = s
alpha = 5;                      % Shift size (# of indices to shift by)
shifts = (1:alpha:N).';         % shifts


%% Adjacency matrix

% for circular indexing...
circidx = @(x) mod(x-1, N) + 1;

% We will first construct an adjacency matrix
adj_mat = sparse(N, N);

for ix = 1:length(shifts)
    ell = shifts(ix)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    adj_mat(j,k) = adj_mat(j,k) + 1;
end

% No. of unknowns
nvars = nnz(adj_mat);

% Order of indexing variables (column major of non-zero entries of above
% adjacency matrix)
[rowidx, colidx] = find(adj_mat);
varorder = sparse(rowidx(:), colidx(:), (1:nvars).', N, N, nvars);


%% Trials loop
for itrial = 1:ntrials

%% Test "Signal" and rank-1 matrix

% Define (Fourier transforms of) true signal - (vector of interest)
xhat = randn(N, 1) + 1i*randn(N, 1);

Xhat = xhat*xhat';          % Rank - 1 matrix

% Add noise
sig_power = ( norm(Xhat(:))^2 ) / numel(Xhat);
noise_power = sig_power / (10^(snr/10));
noise_mat = sqrt(noise_power/2)*( randn(N) + 1i*randn(N) );
Xhat = Xhat + noise_mat;


% Hermitian Symmetrize
Xhat = (Xhat+Xhat')/2;


%% Here is our data matrix

T_X = sparse(N, N);

for ix = 1:length(shifts)
    ell = shifts(ix)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    T_X(j,k) = Xhat(j,k);
end


%% Perform angular synchronization in blocks

tic;

% Initialize solution vector
xhat_rec = zeros(N,1); 

% First block
    % find the block entries
    ell = shifts(1)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    block_entries = T_X(j,k);
    
    % Find e-vector
    [evec, eval] = eigs(block_entries, 1, 'largestabs');

    % Update solution corr. to first block
    u_prev = sqrt(eval)*evec;
    xhat_rec(j) = u_prev;

for ix = 2:length(shifts)
    % In each block...
    
    % find the block entries
    ell = shifts(ix)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    block_entries = T_X(j,k);
    
    % Find e-vector
    [evec, eval] = eigs(block_entries, 1, 'largestabs');
    
    % Scale to correct magnitude
    u = sqrt(eval)*evec;
    
    % Extract overlapping portions
    u_prev_tilde = u_prev(end-(s-alpha)+1:end);
    u_tilde = u(1:(s-alpha));
    
    % Update magnitude of u
    u = ( norm(u_prev_tilde)/norm(u_tilde) )*u;
    
    % Update phase
    u = ( (u_tilde'*u_prev_tilde)/(norm(u_prev_tilde)*norm(u_tilde)) )*u;
    
    % Update components of solution vector
    xhat_rec(j) = u;
    
    % Store current e-vec for next pass
    u_prev = u;    

end

% Store execution time
etime(isval,1) = etime(isval,1) + toc/ntrials;

% ... and error

% Update for global phase factor
phase_factor = xhat_rec'*xhat/(xhat'*xhat);
xhat_rec = xhat_rec*phase_factor;

% Store average error
err = 10*log10( (norm(xhat-xhat_rec)^2) / (norm(xhat)^2) );
error(isval,1) = error(isval,1) + err/ntrials;


end

fprintf( '\n Block Angular Synchronization ' );
fprintf( '\n Avg. ang. sync. time for s=%d is %3.3e', s, etime(isval,1) );
fprintf( '\n Avg. error (dB) for s=%d is %3.3f\n', s, error(isval,1) );
%

end

fprintf( '\n\n' );


% Plot
figure; 
loglog(svals, etime(:,1), '-+', 'linewidth', 2); hold on
loglog(svals(2:3), 2e-3*svals(2:3), ':', 'linewidth', 2);
xlabel('Support Parameter, $s$'); ylabel 'Execution Time (secs.)'
title 'Execution Time - Alg. 2; ($N=1200, \alpha=5$)'
grid on
legend( 'Computed (Block Ang. Sync.)', ...
        '$\mathcal{O}(s)$', 'location', 'northwest')
axis([5e0 500 1e-2 5e-0])

