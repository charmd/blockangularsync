clear; close all; clc

% For pretty pictures
% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',14);
set(0,'defaulttextfontsize',14);
set(0, 'DefaultLineLineWidth', 2);
set(0,'defaultTextInterpreter','latex');
set(0,'defaultLegendInterpreter','latex');


% For each of these N values
Nvals = [50; 150; 300; 600];

% Stores times and errors here
etime = zeros(length(Nvals), 1);
error = zeros(length(Nvals), 1);

% No. of trials
ntrials = 50;

% SNR for added noise
snr = 40;

fprintf( '\n\n' );

for iNval = 1:length(Nvals)

%% Problem Parameters
N = Nvals(iNval);               % Problem/signal size
beta = 3;                       % |supp(m^)| = 2 beta + 1 = s
s = 2*beta+1;
alpha = 5;                      % Shift size (# of indices to shift by)
shifts = (1:alpha:N).';         % Toal # of shifts



%% These are the unknowns/variables we are solving for

% for circular indexing...
circidx = @(x) mod(x-1, N) + 1;

% We will first construct an adjacency matrix
adj_mat = sparse(N, N);

% for ix = 1:alpha:N
for ix = 1:length(shifts)
    ell = shifts(ix)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    adj_mat(j,k) = adj_mat(j,k) + 1;
end

% No. of unknowns
nvars = nnz(adj_mat);

% Order of indexing variables (column major of non-zero entries of above
% adjacency matrix)
[rowidx, colidx] = find(adj_mat);
varorder = sparse(rowidx(:), colidx(:), (1:nvars).', N, N, nvars);


% This is the system matrix
sys_mat = sparse(N*(length(shifts)+1), nvars);
% index variable for complex exponential
nodes = (0:N-1).';


% for ix = 1:alpha:N
for ix = 1:length(shifts)
    ell = shifts(ix)+1;
    j = circidx(ell-beta:ell+beta); k = j;
    
    for ij = 1:length(j)
        for ik = 1:length(k)
            sys_mat((ell-1)*N+1:ell*N,varorder(j(ij), k(ik))) = ...
                                            (randn(N,1) + 1i*randn(N,1));        
        end
    end
end

sys_mat = sys_mat/(N^2);

% dA = sys_mat'*sys_mat;
% Decomposition/factorization
dA = decomposition((ctranspose(sys_mat)*sys_mat), 'qr');


% Define (Fourier transforms of) mask
mhat = zeros(N, 1); 
mhat(1:beta+1) = ones(beta+1, 1);
mhat(end-beta+1:end) = ones(beta, 1);



for itrial = 1:ntrials
%% Test "Signal" and "Mask"

% Define (Fourier transforms of) true signal - (vector of interest)
xhat = randn(N, 1) + 1i*randn(N, 1);


%% Generate measurements

% We'll generate measurements corresponding to each "shift" l separately
Y = zeros(N,length(shifts));

for ishift = 1:length(shifts)
    ell = shifts(ishift)+1;
    Y(:,ell) = abs( ifft( mhat .* circshift(xhat,-(ell-1)) ) ).^2;
end



%% Solve system

rhs = Y(:);

tic;

% Least squares (normal equations)
int_soln = dA\(sys_mat'*rhs);


%% Rearrange into the NxN banded matrix

% Use all variables
Y_tilde = sparse(rowidx(:), colidx(:), int_soln, N, N, nvars);

% Hermitian symmetrize
Y_tilde = (Y_tilde + Y_tilde')/2;


etime(iNval) = etime(iNval) + toc/ntrials;


end


fprintf( '\n Linear system solve time for N=%d is %3.3e', N, etime(iNval,1) );



end


fprintf( '\n\n' );


% Plot
figure; 
loglog(Nvals, etime(:,1), '-+', 'linewidth', 2); hold on
%
loglog(Nvals(2:3), 1e-5*Nvals(2:3), ':', 'linewidth', 2);
loglog(Nvals(2:3), 1.5e-7*Nvals(2:3).^2, ':', 'linewidth', 2);
%
xlabel('Problem Size, $N$'); ylabel 'Execution Time (secs.)'
title 'Execution Time - Alg. 1; ($s=7, \alpha=5$)'
grid
legend( 'Computed (Linear System Solve)', ...
        '$\mathcal{O}(N)$', '$\mathcal{O}(N^2)$', 'location', 'northwest')
axis([20 8e2 3e-4 3e-2]); drawnow

