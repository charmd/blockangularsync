# Block Angular Synchronization with Applications to Ptychographic Phase Retrieval #

### Summary ###

This repository contains MATLAB code implementing a block angular synchronization procedure with applications to Ptychographic Phase Retrieval.


### Contact ###

(Phaseless Imaging Group at University of Michigan - Dearborn)[https://sites.google.com/umich.edu/phaselessimaging]